package entidades;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
 
public class DB {
    private static DB INSTANCE = null;
    //private static String LABASE = "jdbc:hsqldb:file:"+System.getProperty)"user.home")+"/personas,hsqldb";
    private static String LABASE = "jdbc:mysql://localhost/ecommercegrupal";
    private static String LABASEUSUARIO = "root"; //"root";
    private static String LABASECLAVE = ""; //"root";
    
    public static DB getInstance () throws ClassNotFoundException, IOException, SQLException {
           if (INSTANCE == null)  {
               INSTANCE = new DB ();    
           }
           return INSTANCE;
    }
    private DB () throws ClassNotFoundException,
            IOException, SQLException {
}
    
public Connection getConnection ()  throws ClassNotFoundException,
        IOException, SQLException  {
    Class.forName ("org.mariadb.jdbc.Driver");
    return DriverManager.getConnection(LABASE,LABASEUSUARIO,LABASECLAVE);
}

}