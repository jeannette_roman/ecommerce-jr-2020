package web;

import entidades.DB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ConsultaTvs", urlPatterns = {"/ConsultaTvs"})
public class ConsultaTvs extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         PrintWriter out = response.getWriter();
        response.setContentType("text/html;charset=UTF-8");
        out.println("<!DOCTYPE html>\n" +
"<html>\n" +
"<head>\n" +
"    <meta charset='utf-8'>\n" +
"    <meta http-equiv='X-UA-Compatible' content='IE=edge'>\n" +
"    <title>Listado de productos</title>\n" +
"    <meta name='viewport' content='width=device-width, initial-scale=1'>\n" +
"    <link rel='stylesheet' type='text/css' media='screen' href='css/productos.css'>\n" +
"    <script src='main.js'></script>\n" +
"</head>\n" +
"<body>\n" +
"    <header></header>\n" +
" <main>  "
                + "  <div class=\"wrap category-page\" ng-controller=\"Application.Controllers.Category.ProductList\">");
        
        String ConsultaSql = "SELECT * FROM productos ORDER BY id DESC LIMIT 6";
        Connection miConexion = null;
        try {
            miConexion = DB.getInstance().getConnection();
            PreparedStatement miPreparativo = miConexion.prepareStatement(ConsultaSql);
            ResultSet miResultado = miPreparativo.executeQuery ();
            while (miResultado.next())  {
                Producto miProducto = new Producto ();
                miProducto.nombre = miResultado.getString ("nombre");
                miProducto.precio =  miResultado.getString ("precio");
                miProducto.cantidad =  miResultado.getString ("cantidad");
                miProducto.descripcion = miResultado.getString ("descripcion");
                miProducto.foto = miResultado.getString ("foto");
                System.out.println(miProducto);
                /*out.println("<p>" 
                       + miProducto.nombre  + "|" +
                       miProducto.precio + "|" +
                       miProducto.cantidad  + "|" +
                       miProducto.descripcion  + "|" +  
                       " <img src="+ miProducto.foto  + " alt=\rl in n jacket\" width=\"250\" > " +                   
                       "</p>"); */
                
               // out.println("<article>");
               // out.println("<img src=' " + miProducto.foto + " 'width='200' />");
               // out.println("<div>Nombre: " +  miProducto.nombre + "</div>");
               // out.println("<div>Precio: " +  miProducto.precio + "</div>");
               // out.println("<div>Cantidad: " +  miProducto.cantidad + "</div>");
               // out.println("<div>Descripción: " + miProducto.descripcion + "</div>");
                
                // out.println("</article>");
                
                 out.println("<div class=\"products\">\n" +
"        <div class=\"product-box\"\n" +
"             ng-repeat=\"product in products\">\n" +
"          <div class='title'>" +  miProducto.nombre + "</div>\n" +
"          <div class='show-base'>\n" +
"            <img src=\"" + miProducto.foto + "\" alt=\"{{product.name}}\" />\n" +
"            <div class=\"mask\">\n" +
"              <h2>" +  miProducto.nombre + "</h2>\n" +
"              <p ng-class=\"{old:product.specialPrice}\">$" +  miProducto.precio + "</p>\n" +
"              <p ng-show=\"product.specialPrice\">" +  miProducto.cantidad + "</p>\n" +
"              <div class=\"description\">\n" +
"               " + miProducto.descripcion + "\n" +
"              </div>\n" +
"              <div>\n" +
"                <a href=\"#\" class=\"more\">Info</a>\n" +
"                <a href=\"#\" class=\"tocart\">Comprar</a>\n" +
"              </div>\n" +
"            </div>\n" +
"          </div>\n" +
"         </div>\n" +
"      </div>");
                                  
            }
            out.println("</div>");
               
            
        } catch (ClassNotFoundException miExepcion) {
            System.out.println(miExepcion);
        } catch (SQLException miExepcion) {
            System.out.println(miExepcion);
        } finally {
            try {
                
               
                if (miConexion != null) {
                    miConexion.close();
                } 
                
            } catch (SQLException miExepcion) {
                System.out.println(miExepcion);
            }
        }

        out.println("</main>\n" +
"    <footer></footer>\n" +
"</body>\n" +
"</html>");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    @Override
    public String getServletInfo() {
        return "Short description";

    }

}
